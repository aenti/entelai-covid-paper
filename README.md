# COVID-19 Pneumonia accurately detected in chest X-rays with artificial intelligence Code #

This repository contains the non-privative code used to train the article model. 

### Training ###

* Open Training notebook
* Replace data and pretrained model with your own
* Run cells

### Prediction ###

* Open Prediction notebook
* Run predict function with your trained model and given picture
* Activation MAP: a function is provided to obtain an activation map from the average pooling layer